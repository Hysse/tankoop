﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCameraSingleton : Singleton<MainCameraSingleton>
{
    // Class only needed to implement singleton
}
