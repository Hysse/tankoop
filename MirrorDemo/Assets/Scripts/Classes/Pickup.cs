﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pickup : MonoBehaviour
{
    public enum TYPE { Coin };
    public TYPE type;

    private int id;
    private float benefitValue; // Changes meaning depending on the type

    public int getId()
    {
        return this.id;
    }

    public float getBenefitValue()
    {
        return this.benefitValue;
    }

    public void setBenefitValue(float newBenefitValue)
    {
        this.benefitValue = newBenefitValue;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public override bool Equals(object obj)
    {
        Pickup cat = obj as Pickup;
        if (cat == null)
            return false;
        else
            return base.Equals(obj) && this.id == cat.id;
    }

    public override int GetHashCode()
    {
        return id;
    }
}

