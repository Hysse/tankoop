﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Catastrophe : MonoBehaviour
{
    public enum TYPE {Flame, Meteor, Seeker, Mine};
    public TYPE type;

    private int id;
    private float damage;

    public int getId()
    {
        return this.id;
    }

    public float getDamage()
    {
        return this.damage;
    }
    
    public void setDamage(float newDamage)
    {
        this.damage = newDamage;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public override bool Equals(object obj)
    {
        Catastrophe cat = obj as Catastrophe;
        if (cat == null)
            return false;
        else
            return base.Equals(obj) && this.id == cat.id;
    }

    public override int GetHashCode()
    {
        return id;
    }
}

