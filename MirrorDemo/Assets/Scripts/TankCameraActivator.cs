﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class TankCameraActivator : NetworkBehaviour
{
    public Camera my_cam;
    public AudioListener my_audio;

    [Client]
    void Update()
    {
        if (my_cam != null && my_audio != null)
        {
            if (!isLocalPlayer)
            {
                my_cam.enabled = false;
                my_audio.enabled = false;
            }
            else
            {
                my_cam.enabled = true;
                my_audio.enabled = true;
            }
        }
    }
}
