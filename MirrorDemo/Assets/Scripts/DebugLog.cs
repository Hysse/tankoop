﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DebugLog : MonoBehaviour
{
    public Text logtxt;
    public GameObject lifebar;
    
    // Start is called before the first frame update
    void Start()
    {
        logtxt.text = "lifebar obj : " + lifebar + "lifebar image : " + lifebar.GetComponent<Image>();
        Image VRHealthBar = GameObject.FindWithTag("VRLifebar").GetComponent<Image>();
        logtxt.text += "\nsearch result: " + VRHealthBar;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
