﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VRGameOverMenu : Singleton<VRGameOverMenu>
{
    private void Start()
    {
        gameObject.GetComponent<Canvas>().worldCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
    }
}
