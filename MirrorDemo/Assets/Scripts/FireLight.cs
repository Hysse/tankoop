﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireLight : MonoBehaviour
{
    Light light;

    [Range(10, 60)]
    public int frameUntilChange;

    int count;

    // Start is called before the first frame update
    void Start()
    {
        light = gameObject.GetComponent<Light>();
        count = 0;
    }

    // Update is called once per frame
    void Update()
    {
        count++;
        if (count == frameUntilChange)
        {
            light.intensity = Random.Range(50, 100);
            count = 0;
        }
    }
}
