﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class BulletControl : NetworkBehaviour
{

    public float maxDistance;
    public float forwardSpeed;

    public CatastropheSpawner spawner;

    void Start()
    {
        if(isServer)
        {
            spawner = GameObject.FindWithTag("CatSpawner").GetComponent<CatastropheSpawner>();
        }
    }

    void Update()
    {
        if(gameObject.transform.position.x >= maxDistance || gameObject.transform.position.x <= -maxDistance || gameObject.transform.position.z >= maxDistance || gameObject.transform.position.z <= -maxDistance)
        {
            Destroy(gameObject);
        }
        transform.Translate(new Vector3(0f, 0f, forwardSpeed * Time.deltaTime), Space.Self);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Catastrophe") && isServer && other.gameObject.GetComponent<Catastrophe>().type == Catastrophe.TYPE.Seeker)
        {
            spawner.deleteCatastrophe(other.gameObject.GetComponent<Catastrophe>().getId());
            NetworkServer.Destroy(gameObject);
;        }
    }
}
