﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PCCameraController : MonoBehaviour
{

    public GameObject camera_center;

    // Start is called before the first frame update
    void Start()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            this.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            float horizontalSpeed = Input.GetAxis("Mouse X");
            gameObject.transform.RotateAround(camera_center.transform.position, new Vector3(0f, 1f, 0f), horizontalSpeed);
        }
    }
}
