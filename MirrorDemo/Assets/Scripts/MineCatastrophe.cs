﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class MineCatastrophe : NetworkBehaviour
{

    [Tooltip("The range at which the mine can be seen")]
    [Range(5f, 200f)]
    public float vision_range = 50f;

    public GameObject body;
    public GameObject lightBulb;
    public Light flashLight;
    public GameObject explosion;
    public GameObject target;

    CatastropheSpawner spawner;
    PlayerController playerController;

    float lightMaxIntensity;
    Renderer body_renderer;
    Renderer bulb_renderer;
    GameObject player;

    // Start is called before the first frame update
    void Start()
    {
        if (body)
        {
            body_renderer = body.transform.GetComponent<Renderer>();
        }
        if (lightBulb)
        {
            bulb_renderer = lightBulb.transform.GetComponent<Renderer>();
        }
        if (flashLight)
        {
            lightMaxIntensity = flashLight.intensity;
        }
        player = GameObject.FindWithTag("Player");
        if (isServer)
        {
            spawner = GameObject.FindWithTag("CatSpawner").GetComponent<CatastropheSpawner>();
        }
        playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player)
        {
            float distance = Vector2.Distance(new Vector2(transform.position.x, transform.position.z), new Vector2(player.transform.position.x, player.transform.position.z));
            float visibility;
            float newIntensiy;
            if (distance <= vision_range)
            {
                //CALCULATES THE VISIBILITY OF THE MINE
                float range_percentage = distance / vision_range; 
                // range_percentage :  1f at range distance, 0f at player position
                visibility = Mathf.Lerp(1f, 0f, range_percentage);
                newIntensiy = Mathf.Lerp(lightMaxIntensity, 0f, range_percentage);
            }
            else
            {
                visibility = 0f;
                newIntensiy = 0f;
            }
            if (body_renderer)
            {
                body_renderer.material.color = new Color(body_renderer.material.color.r, body_renderer.material.color.g, body_renderer.material.color.b, visibility);
            }
            if (bulb_renderer)
            {
                bulb_renderer.material.color = new Color(bulb_renderer.material.color.r, bulb_renderer.material.color.g, bulb_renderer.material.color.b, visibility);
            }
            if (flashLight)
            {
                flashLight.intensity = newIntensiy;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            explosion.SetActive(true);
            lightBulb.SetActive(false);
            body.GetComponent<Renderer>().enabled = false;
            gameObject.GetComponent<MineCatastrophe>().enabled = false;
            gameObject.GetComponent<BoxCollider>().enabled = false;
            if (isServer)
            {
                playerController.takeDamage(gameObject.GetComponent<Catastrophe>().getDamage());
                NetworkServer.Destroy(target);
                StartCoroutine(TankoopUtils.destroyCoroutine(gameObject, 2, spawner));
            }
        }
    }
}
