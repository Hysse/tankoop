﻿using Mirror;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Player : NetworkBehaviour
{
    [SerializeField] private float movSpeed;
    [SerializeField] private float rotSpeed;


    // Start is called before the first frame update
    void Start()
    {
        if (movSpeed == 0f)
        {
            movSpeed = 5f;
        }
    }

    [Client]
    void Update()
    {
        if (!hasAuthority)
        { return; }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            gameObject.transform.Rotate(new Vector3(0, -rotSpeed * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            gameObject.transform.Rotate(new Vector3(0, rotSpeed * Time.deltaTime, 0));
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.Translate(transform.forward * Time.deltaTime * movSpeed);
        }
    }
}
