﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class CameraActivator : NetworkBehaviour
{
    public Camera my_cam;
    public AudioListener my_audio;

    [Client]
    // Update is called once per frame
    void Update()
    {
        if (!isLocalPlayer)
        {
            my_cam.enabled = false;
            my_audio.enabled = false;
        }
        else
        {
            my_cam.enabled = true;
            my_audio.enabled = true;
        }
    }
}
