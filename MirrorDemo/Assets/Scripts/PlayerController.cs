﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.UI;

public class PlayerController : NetworkBehaviour
{

    [Tooltip("The starting life of the Player")]
    public float startHealth = 100;

    [Tooltip("The UI Image element that displays the Player's health on PC")]
    public Image PCHealthBar;

    [Tooltip("The UI Text element that displays the Player's score on PC")]
    public Text PCScoreTxt;

    Image VRHealthBar;
    Text VRScoreTxt;
    bool can_regen = false;

    [SyncVar(hook = "OnHealthChanged")]
    private float currentHealth;

    [SyncVar(hook = "OnScoreChanged")]
    private float currentScore;

    [Server]
    void Start()
    {
        currentScore = 0;
        currentHealth = startHealth;
        PCScoreTxt.text = "" + 0;
    }

    private void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if(VRHealthBar == null)
            {
                VRHealthBar = GameObject.FindWithTag("VRLifebar").GetComponent<Image>();
            }
            if (VRScoreTxt == null)
            {
                VRScoreTxt = GameObject.FindWithTag("ScoreTxt").GetComponent<Text>();
                if(VRScoreTxt != null)
                {
                    VRScoreTxt.text = "" + 0;
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            this.takeDamage(150);
        }
    }

    [Server]
    public void takeDamage(float damage)
    {
        currentHealth -= damage;
        if (currentHealth <= 0f)
        {
            RpcDie();
        }
        PCHealthBar.fillAmount = currentHealth / startHealth;
    }

    [Server]
    public void gainPoints(float amount)
    {
        currentScore += amount;
        PCScoreTxt.text = "" + currentScore;
    }

    [ClientRpc(excludeOwner = false)]
    void RpcDie()
    {
        GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gameManager.RpcStopGame();
        if(Application.platform == RuntimePlatform.Android)
        {
            gameManager.activateVRGameOverMenu(currentScore);
        } else
        {
            gameManager.activatePCGameOverMenu(currentScore);
        }
    }

    void OnHealthChanged(float oldValue, float newValue)
    {
        if (VRHealthBar != null)
        {
            VRHealthBar.fillAmount = newValue / startHealth;
        }
    }

    void OnScoreChanged(float oldValue, float newValue)
    {
        if (VRScoreTxt != null)
        {
            VRScoreTxt.text = "" + newValue;
        }
    }

    [Server]
    public void heal(float amount)
    {
        currentHealth += amount;
        if (currentHealth > startHealth)
        {
            currentHealth = startHealth;
        }
        PCHealthBar.fillAmount = currentHealth / startHealth;
    }

    [Server]
    public void losePoints(float amount)
    {
        currentScore -= amount;
        PCScoreTxt.text = "" + currentScore;
    }

    public float getCurrentHealth()
    {
        return currentHealth;
    }

    public float getCurrentScore()
    {
        return currentScore;
    }
}
