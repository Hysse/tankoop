﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using UnityEngine.XR;
using UnityEngine.UI;
using System;

public class HandHeldController : NetworkBehaviour
{
    public Text logText;
    public GameObject tank;
    public GameObject bullet_spawn;

    List<InputDevice> devices;
    InputDevice device;
    TanksNetworkManager manager;
    bool cannon_loaded = true;

    private void Start()
    {
        manager = GameObject.Find("TanksNetworkManager").GetComponent<TanksNetworkManager>();
        bullet_spawn = GameObject.Find("BulletSpawn");
        devices = new List<InputDevice>();
        InputDevices.GetDevicesWithCharacteristics(InputDeviceCharacteristics.Right | InputDeviceCharacteristics.HeldInHand | InputDeviceCharacteristics.Controller, devices);
        if (devices.Count > 0)
        {
            device = devices[0];
        }
    }

    void Update()
    {
        if (device != null)
        {
            bool triggerValue;
            if (device.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out triggerValue) && triggerValue)
            {
                Fire();
            }
        }
    }

    void Fire()
    {
        if (cannon_loaded && bullet_spawn)
        {
            cannon_loaded = false;
            CmdFire();
            StartCoroutine(ReloadCannonCoroutine());
        }
    }

    [Command]
    void CmdFire()
    {
        if (cannon_loaded && bullet_spawn)
        {
            GameObject bullet = Instantiate(manager.spawnPrefabs.Find(prefab => prefab.name == "Bullet"), bullet_spawn.transform.position, bullet_spawn.transform.rotation);
            NetworkServer.Spawn(bullet);
            StartCoroutine(ReloadCannonCoroutine());
        }
    }

    IEnumerator ReloadCannonCoroutine()
    {
        yield return new WaitForSeconds(2);
        cannon_loaded = true;
    }
}
