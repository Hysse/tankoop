﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class PickupSpawner : NetworkBehaviour
{
    [Tooltip("List of Pickup Prefabs that can be spawned by the CatastropheSpawner")]
    public List<GameObject> pickupPrefabs = new List<GameObject>();

    [Tooltip("Seconds after which Pickups will begin to spawn")]
    [Range(0, 60)]
    public int initialDelay = 0;

    [Tooltip("Number of seconds between the spawning of two Pickups")]
    [Range(1, 5)]
    public int betweenDelay = 1;

    [Tooltip("Maximum amount of Pickups active at once")]
    [Range(1, 20)]
    public int maxPickups = 5;

    [Tooltip("The radius of the circular area in which the PickupSpawner is active")]
    public float areaRadius = 20;

    public GameObject tank;

    bool canSpawn = false;
    Coroutine coroutine;
    int randIndex;
    int incrementalId;

    //List containing references to the spawned catastrophes
    List<GameObject> pickups = new List<GameObject>();

    private void Start()
    {
        incrementalId = 0;
    }

    [Server]
    void Update()
    {
        if (canSpawn == true && pickups.Count < maxPickups)
        {
            canSpawn = false;
            SpawnPickup(PickupRandPosition(out randIndex));
            coroutine = StartCoroutine(delayCoroutine(betweenDelay));
        }
    }


    IEnumerator delayCoroutine(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        canSpawn = true;
    }

    Vector2 PickupRandPosition(out int random_index)
    {
        //Creating random point in spawn area circle for the Pickup Instantiate
        random_index = Random.Range(0, pickupPrefabs.Count);
        GameObject pickup = pickupPrefabs[random_index];
        float cat_angle, cat_r, cat_x = 0, cat_z = 0;
        switch (pickup.GetComponent<Pickup>().type)
        {
            case Pickup.TYPE.Coin:
                {
                    if (tank != null)
                    {
                        float x_size = tank.GetComponent<BoxCollider>().size.x + 5f;
                        float z_size = tank.GetComponent<BoxCollider>().size.z + 5f;
                        bool acceptable = false;
                        while (!acceptable)
                        {
                            cat_angle = Random.value * 2 * Mathf.PI;
                            cat_r = areaRadius * Mathf.Sqrt(Random.value);
                            cat_x = cat_r * Mathf.Cos(cat_angle);
                            cat_z = cat_r * Mathf.Sin(cat_angle);
                            //CHECKS IF THE CHOSEN SPAWN POSITION DOES NOT INTERSECT WITH THE BOX COLLIDER OF THE TANK
                            if (!(((cat_x >= tank.transform.position.x - (x_size / 2)) && (cat_x <= tank.transform.position.x + (x_size / 2))) && ((cat_z >= tank.transform.position.z - (z_size / 2)) && (cat_z <= tank.transform.position.z + (z_size / 2)))))
                            {
                                acceptable = true;
                            }
                        }
                    }
                    else
                    {
                        cat_angle = Random.value * 2 * Mathf.PI;
                        cat_r = areaRadius * Mathf.Sqrt(Random.value);
                        cat_x = cat_r * Mathf.Cos(cat_angle);
                        cat_z = cat_r * Mathf.Sin(cat_angle);
                    }
                    break;
                }
            default:
                {
                    cat_angle = Random.value * 2 * Mathf.PI;
                    cat_r = areaRadius * Mathf.Sqrt(Random.value);
                    cat_x = cat_r * Mathf.Cos(cat_angle);
                    cat_z = cat_r * Mathf.Sin(cat_angle);
                    break;
                }
        }
        return new Vector2(cat_x, cat_z);
    }

    void SpawnPickup(Vector2 cat_pos)
    {
        float y_pos;
        float benefitValue = 0f;
        Quaternion rotation;
        switch (pickupPrefabs[randIndex].GetComponent<Pickup>().type)
        {
            case Pickup.TYPE.Coin:
                {
                    y_pos = 0f;
                    rotation = Quaternion.identity;
                    benefitValue = 50f;
                    break;
                }
            default:
                {
                    y_pos = 0f;
                    rotation = Quaternion.identity;
                    break;
                }
        }
        //Instantiating the Pickup
        GameObject pickup = Instantiate(pickupPrefabs[randIndex], new Vector3(cat_pos.x, y_pos, cat_pos.y), rotation);
        pickup.GetComponent<Pickup>().setId(incrementalId);
        pickup.GetComponent<Pickup>().setBenefitValue(benefitValue);
        incrementalId++;
        pickups.Add(pickup);
        NetworkServer.Spawn(pickup);
    }

    public void startSpawning()
    {
        coroutine = StartCoroutine(delayCoroutine(initialDelay));
    }

    public void stopSpawning()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        canSpawn = false;
    }

    public void deletePickup(int id)
    {
        GameObject toRemove = null;
        pickups.ForEach((pickup) =>
        {
            //Debug.Log("pickup id : " + pickup.GetComponent<Catastrophe>().getId() + "\ntoDelete.Id : " + toDelete.getId());
            if (pickup.GetComponent<Pickup>().getId() == id)
            {
                toRemove = pickup;
            }
        });
        if (toRemove != null)
        {
            pickups.Remove(toRemove);
            NetworkServer.Destroy(toRemove);
        }
    }
}
