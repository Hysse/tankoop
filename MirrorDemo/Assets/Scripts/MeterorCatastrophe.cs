﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
public class MeterorCatastrophe : NetworkBehaviour
{

    [Tooltip("The number of seconds of delay between two flashes of the countdown light")]
    [Range(0.1f, 5f)]
    public float flashDelay = 1;

    [Tooltip("The number of seconds from the start of the countdown to the explosion of the Meteor")]
    [Range(1, 5)]
    public int meteorCountdown;

    public GameObject meteor;

    public GameObject explosion;

    public GameObject target_light;

    public CatastropheSpawner catSpawner;

    Coroutine flash_coroutine;
    Coroutine destroy_coroutine;
    PlayerController playerController;
    Renderer target_renderer;
    bool delayElapsed = true;
    Color startingColor;
    float fallingSpeed;
    float count = 0f;

    // Start is called before the first frame update
    void Start()
    {
        target_renderer = gameObject.transform.GetComponent<Renderer>();
        startingColor = target_renderer.material.GetColor("_Color");
        target_light.SetActive(false);
        gameObject.GetComponent<SphereCollider>().enabled = false;
        launchMeteor();
        playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (count >= meteorCountdown)
        {
            Destroy(meteor);
            Destroy(target_light);
            target_renderer.enabled = false;
            explosion.SetActive(true);
            gameObject.GetComponent<SphereCollider>().enabled = true;
            StopCoroutine(flash_coroutine);
            if (isServer)
            {
                destroy_coroutine = StartCoroutine(TankoopUtils.destroyCoroutine(gameObject, 2, catSpawner));
            }
        }
        else
        {
            count += Time.deltaTime;
        }
        if (delayElapsed)
        {
            delayElapsed = false;
            flash_coroutine = StartCoroutine(flashCoroutine(flashDelay));
        }
    }

    IEnumerator flashCoroutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (target_renderer.material.color.g == 0f)
        {
            target_light.SetActive(false);
            target_renderer.material.color = startingColor;
        }
        else
        {
            target_light.SetActive(true);
            target_renderer.material.color = new Color(100f, 0f, 0f, 255f);
        }
        delayElapsed = true;
    }

    void launchMeteor()
    {
        float distance = Vector3.Distance(meteor.transform.position, gameObject.transform.position);
        float speed = distance / meteorCountdown;
        meteor.GetComponent<Rigidbody>().velocity = new Vector3(0f, -speed, 0f);
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")){
            if (isServer)
            {
                playerController.takeDamage(gameObject.GetComponent<Catastrophe>().getDamage());
            }
        }
    }
}
