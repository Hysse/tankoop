﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekerBlock : MonoBehaviour
{
    [Tooltip("The refresh rate of the texture on the catastrophe")]
    [Range(0.1f, 1f)]
    public float refreshRate = 1f;

    //public Text logtxt;
    Renderer renderer;
    bool can_change = true;
    // Start is called before the first frame update
    void Start()
    {
        renderer = gameObject.transform.GetComponent<Renderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (can_change && renderer.enabled)
        {
            can_change = false;
            Vector2 randOffset = new Vector2(Random.value, Random.value);
            Vector2 randTiling = new Vector2(Random.Range(1, 6), Random.Range(1, 6));
            renderer.material.mainTextureOffset = randOffset;
            renderer.material.mainTextureScale = randTiling;
            StartCoroutine(delayChange(refreshRate));
        }
    }

    IEnumerator delayChange(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        can_change = true;
    }
}
