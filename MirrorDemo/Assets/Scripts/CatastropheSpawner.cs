﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class CatastropheSpawner : NetworkBehaviour
{
    [Tooltip("List of Catastrophe Prefabs that can be spawned by the CatastropheSpawner")]
    public List<GameObject> catastrophePrefabs = new List<GameObject>();

    [Tooltip("List of materials that can be set onto the targets depending on which Catastrophe the targets are connected to")]
    public List<GameObject> targetPrefabs = new List<GameObject>();

    [Tooltip("Seconds after which Catastrophes will begin to spawn")]
    [Range(0, 60)]
    public int initialDelay = 0;

    [Tooltip("Number of seconds between the spawning of two Catastrophes")]
    [Range(1, 5)]
    public int betweenDelay = 1;

    [Tooltip("Maximum amount of Catastrophes active at once")]
    [Range(1, 20)]
    public int maxCatastrophes = 5;

    [Tooltip("The radius of the circular area in which the CatastropheSpawner is active")]
    public float areaRadius = 20;

    [Tooltip("The objects that indicates the height and the radius of the outer circle where targets are spawned")]
    public Transform outerRingObject;

    [Tooltip("The target prefab that is going to be spawned with the Catastrophes")]
    public GameObject targetPrefab;

    public GameObject tank;

    bool canSpawn = false;
    Coroutine coroutine;
    int randIndex;
    int incrementalId;

    //List containing references to the spawned catastrophes
    List<GameObject> catastrophes = new List<GameObject>();

    private void Start()
    {
        incrementalId = 0;
    }

    void Update()
    {
        if (canSpawn == true && catastrophes.Count < maxCatastrophes)
        {
            canSpawn = false;
            SpawnCatastrophe(CatastropheRandPosition(out randIndex), TargetRandPosition());
            coroutine = StartCoroutine(delayCoroutine(betweenDelay));
        }
    }


    IEnumerator delayCoroutine(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        canSpawn = true;
    }

    Vector2 CatastropheRandPosition(out int random_index)
    {
        //Creating random point in spawn area circle for the Catastrophe Instantiate
        random_index = Random.Range(0, catastrophePrefabs.Count);
        GameObject catastrophe = catastrophePrefabs[random_index];
        float cat_angle, cat_r, cat_x = 0, cat_z = 0;
        switch (catastrophe.GetComponent<Catastrophe>().type)
        {
            case Catastrophe.TYPE.Seeker:
                {
                    cat_angle = Random.value * 2 * Mathf.PI;
                    cat_r = areaRadius;
                    cat_x = cat_r * Mathf.Cos(cat_angle);
                    cat_z = cat_r * Mathf.Sin(cat_angle);
                    break;
                }
            case Catastrophe.TYPE.Mine:
                {
                    if (tank != null)
                    {
                        float x_size = tank.GetComponent<BoxCollider>().size.x + 5f;
                        float z_size = tank.GetComponent<BoxCollider>().size.z + 5f;
                        bool acceptable = false;
                        while (!acceptable)
                        {
                            cat_angle = Random.value * 2 * Mathf.PI;
                            cat_r = areaRadius * Mathf.Sqrt(Random.value);
                            cat_x = cat_r * Mathf.Cos(cat_angle);
                            cat_z = cat_r * Mathf.Sin(cat_angle);
                            //CHECKS IF THE CHOSEN SPAWN POSITION DOES NOT INTERSECT WITH THE BOX COLLIDER OF THE TANK
                            if (!(((cat_x >= tank.transform.position.x - (x_size / 2)) && (cat_x <= tank.transform.position.x + (x_size / 2))) && ((cat_z >= tank.transform.position.z - (z_size / 2)) && (cat_z <= tank.transform.position.z + (z_size / 2)))))
                            {
                                acceptable = true;
                            }
                        }
                    }
                    else
                    {
                        cat_angle = Random.value * 2 * Mathf.PI;
                        cat_r = areaRadius * Mathf.Sqrt(Random.value);
                        cat_x = cat_r * Mathf.Cos(cat_angle);
                        cat_z = cat_r * Mathf.Sin(cat_angle);
                    }
                    break;
                }
            default:
                {
                    cat_angle = Random.value * 2 * Mathf.PI;
                    cat_r = areaRadius * Mathf.Sqrt(Random.value);
                    cat_x = cat_r * Mathf.Cos(cat_angle);
                    cat_z = cat_r * Mathf.Sin(cat_angle);
                    break;
                }
        }
        return new Vector2(cat_x, cat_z);
    }

    Vector2 TargetRandPosition()
    {
        //Creating random point in spawn outer circle for the Target Instantiate
        float tar_angle = Random.value * 2 * Mathf.PI;
        float tar_r = Vector2.Distance(new Vector2(outerRingObject.position.x, outerRingObject.position.z), new Vector2(gameObject.transform.position.x, gameObject.transform.position.z));
        float tar_x = tar_r * Mathf.Cos(tar_angle);
        float tar_z = tar_r * Mathf.Sin(tar_angle);
        return new Vector2(tar_x, tar_z);
    }

    GameObject getTargetPrefab(string name, GameObject defaultTarget)
    {
        GameObject toReturn = defaultTarget; ;
        targetPrefabs.ForEach((target) => {
            if(target.name.Equals(name))
            {
                toReturn = target;
            }
        });
        return toReturn;
    }

    void SpawnCatastrophe(Vector2 cat_pos, Vector2 tar_pos)
    {
        bool needTarget = true; //Indicates whether the spawned Catastrophe has to be tied to a Target or not.
        float y_pos;
        float damage = 0f;
        Quaternion rotation;
        switch (catastrophePrefabs[randIndex].GetComponent<Catastrophe>().type)
        {
            case Catastrophe.TYPE.Seeker:
                {
                    needTarget = false;
                    y_pos = 0.1f;
                    damage = 10f;
                    rotation = Quaternion.identity;
                    break;
                }
            case Catastrophe.TYPE.Meteor:
                {
                    needTarget = false;
                    y_pos = 0.2f;
                    rotation = Quaternion.identity;
                    damage = 30f;
                    break;
                }
            case Catastrophe.TYPE.Flame:
                {
                    needTarget = true;
                    y_pos = 0.1f;
                    rotation = Quaternion.identity;
                    damage = 0.5f;
                    break;
                }
            case Catastrophe.TYPE.Mine:
                {
                    needTarget = true;
                    y_pos = 0f;
                    rotation = Quaternion.Euler(0f, 0f, 0f);
                    damage = 10f;
                    break;
                }
            default:
                {
                    y_pos = 0.1f;
                    rotation = Quaternion.identity;
                    break;
                }
        }
        //Instantiating the Catastrophe
        GameObject catastrophe = Instantiate(catastrophePrefabs[randIndex], new Vector3(cat_pos.x, y_pos, cat_pos.y), rotation);
        catastrophe.GetComponent<Catastrophe>().setId(incrementalId);
        catastrophe.GetComponent<Catastrophe>().setDamage(damage);
        incrementalId++;
        catastrophes.Add(catastrophe);
        if (needTarget)
        {
            GameObject toSpawn = targetPrefab;
            //Instantiating the Target Prefab depending on the Catastrophe Type
            switch (catastrophe.GetComponent<Catastrophe>().type)
            {
                case Catastrophe.TYPE.Mine:
                    {
                        toSpawn = getTargetPrefab("MineTarget", targetPrefab);
                        break;
                    }
                case Catastrophe.TYPE.Flame:
                    {
                        toSpawn = getTargetPrefab("FireTarget", targetPrefab);
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            GameObject target = Instantiate(toSpawn, new Vector3(tar_pos.x, outerRingObject.position.y, tar_pos.y), Quaternion.identity);
            TargetController tar_controller = target.GetComponent<TargetController>();
            tar_controller.catastrophe = catastrophe;
            tar_controller.catSpawner = gameObject.GetComponent<CatastropheSpawner>();
            switch (catastrophe.GetComponent<Catastrophe>().type)
            {
                case Catastrophe.TYPE.Mine:
                    {
                        MineCatastrophe mine_cat = catastrophe.GetComponent<MineCatastrophe>();
                        mine_cat.target = target;
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
            NetworkServer.Spawn(target);
        }
        else
        {
            switch (catastrophe.GetComponent<Catastrophe>().type)
            {
                case Catastrophe.TYPE.Meteor:
                    {
                        catastrophe.GetComponent<MeterorCatastrophe>().catSpawner = gameObject.GetComponent<CatastropheSpawner>();
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }
        NetworkServer.Spawn(catastrophe);
    }

    public void startSpawning()
    {
        coroutine = StartCoroutine(delayCoroutine(initialDelay));
    }

    public void stopSpawning()
    {
        if (coroutine != null)
        {
            StopCoroutine(coroutine);
        }
        canSpawn = false;
    }


    public void deleteCatastrophe(int id)
    {
        GameObject toRemove = null;
        catastrophes.ForEach((cat) =>
        {
            //Debug.Log("cat id : " + cat.GetComponent<Catastrophe>().getId() + "\ntoDelete.Id : " + toDelete.getId());
            if (cat.GetComponent<Catastrophe>().getId() == id)
            {
                toRemove = cat;
            }
        });
        if (toRemove != null)
        {
            catastrophes.Remove(toRemove);
            NetworkServer.Destroy(toRemove);
        }
    }
}
