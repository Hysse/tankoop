﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollisions : MonoBehaviour
{
    PlayerController playerController;

    public void Start()
    {
        playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Catastrophe"))
        {
            Catastrophe catastrophe = other.GetComponent<Catastrophe>();
            playerController.takeDamage(catastrophe.getDamage());
        }
    }
}
