﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class TargetController : NetworkBehaviour
{
    [Tooltip("The object towards which this target will be faced (Usually the player)")]
    public GameObject target;

    [Tooltip("The catastrophe associated with this Target")]
    public GameObject catastrophe;

    public CatastropheSpawner catSpawner;

    void Start()
    {
        if (target == null)
        {
            target = GameObject.FindWithTag("Player");
        }
    }

    void Update()
    {
        if(target != null)
        {
            gameObject.transform.LookAt(target.transform);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(!isServer)
        {
            return;
        }

        if(collision.gameObject.CompareTag("Bullet"))
        {
            catSpawner.deleteCatastrophe(catastrophe.GetComponent<Catastrophe>().getId());
            NetworkServer.Destroy(gameObject);
        }
    }
}
