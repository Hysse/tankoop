﻿using UnityEngine;
using System.Collections;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{

    private static T _instance;

    public static T instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<T>();

                // Tell unity not to destroy this object when loading a new scene
                DontDestroyOnLoad(_instance.gameObject);
            }

            return _instance;
        }
    }
    public static bool HasInstance
    {
        get
        {
            return _instance != null;
        }
    }

    protected virtual void Awake()
    {
        if (_instance == null)
        {
            // If I am the first instance, make me the Singleton
            _instance = this as T;
            DontDestroyOnLoad(this);
        }
        else
        {
            // If a Singleton already exists and you find
            // another reference in scene, destroy it!
            if (this != _instance)
                Destroy(this.gameObject);
        }
    }
}