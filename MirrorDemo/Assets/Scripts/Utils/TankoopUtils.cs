﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class TankoopUtils : NetworkBehaviour
{
    public static IEnumerator destroyCoroutine(GameObject obj, float seconds, CatastropheSpawner spawner)
    {
        yield return new WaitForSeconds(seconds);
        spawner.deleteCatastrophe(obj.GetComponent<Catastrophe>().getId());
    }
}
