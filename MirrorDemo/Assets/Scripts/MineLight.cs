﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MineLight : MonoBehaviour
{

    [Tooltip("The amount of delay between light flashes")]
    [Range(0.1f, 3f)]
    public float flash_speed = 1f;

    public Light flashLight;

    Renderer light_renderer;
    bool can_change = true;
    // Start is called before the first frame update
    void Start()
    {
        light_renderer = gameObject.GetComponent<Renderer>();
        light_renderer.material.color = new Color(light_renderer.material.color.r, light_renderer.material.color.g, light_renderer.material.color.b, 0f);
        if(flashLight)
        {
            flashLight.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(can_change)
        {
            can_change = false;
            StartCoroutine(flashRoutine(flash_speed));
        }
    }

    IEnumerator flashRoutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (light_renderer != null)
        {
            if (light_renderer.enabled)
            {
                light_renderer.enabled = false;
                if (flashLight)
                {
                    flashLight.enabled = false;
                }
            }
            else
            {
                light_renderer.enabled = true;
                if (flashLight)
                {
                    flashLight.enabled = true;
                }
            }
        }
            can_change = true;
    }
}
