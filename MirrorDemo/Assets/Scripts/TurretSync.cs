﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretSync : MonoBehaviour
{

    public GameObject turret;
    public GameObject soldier_cam;
    public GameObject tank;

    float currentZeroOfTank;
    float leftLimit;
    float rightLimit;
    float currentZeroOfCamera;

    // Update is called once per frame
    void Update()
    {
        if (turret && tank)
        {
            currentZeroOfTank = tank.transform.rotation.eulerAngles.y;
            leftLimit = currentZeroOfTank - 45;
            rightLimit = currentZeroOfTank + 45;
            currentZeroOfCamera = soldier_cam.transform.rotation.eulerAngles.y;
            if (isInBound())
            {
                turret.transform.rotation = Quaternion.Euler(90, 0, -soldier_cam.transform.rotation.eulerAngles.y - 90);
            }
        }
    }

    //Checks if the final position of the turret after the sync with the VR rig rotation would exceed the boundaries of the 90° cone in front of the tank
    private bool isInBound()
    {
        bool isinbound = true;
        if (leftLimit < 0)
            leftLimit = leftLimit + 360;
        if (rightLimit >= 360)
            rightLimit = rightLimit - 360;
        if ((rightLimit < leftLimit) && (currentZeroOfCamera > rightLimit && currentZeroOfCamera < leftLimit))
            isinbound = false;
        if ((rightLimit > leftLimit) && !(currentZeroOfCamera < rightLimit && currentZeroOfCamera > leftLimit))
            isinbound = false;
        return isinbound;
    }
}
