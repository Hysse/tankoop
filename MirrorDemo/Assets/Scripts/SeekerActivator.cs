﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeekerActivator : MonoBehaviour
{

    public GameObject seeker_block;
    public GameObject heat;

    public void Start()
    {
        if(Application.platform != RuntimePlatform.Android)
        {
            this.deactivate();
        }
    }
    public void deactivate()
    {
        seeker_block.GetComponent<Renderer>().enabled = false;
        heat.SetActive(false);
    }

    public void activate()
    {
        seeker_block.GetComponent<Renderer>().enabled = true;
        heat.SetActive(true);
    }


}
