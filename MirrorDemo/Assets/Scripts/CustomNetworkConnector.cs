﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Mirror;

public class CustomNetworkConnector : MonoBehaviour
{
    NetworkManager manager;
    Camera mainCamera;
    AudioListener mainAudio;

    public PCCameraController camera_controller;
    public string addressForClient;


    private void Start()
    {
        manager = GameObject.Find("TanksNetworkManager").GetComponent<NetworkManager>();
        camera_controller.enabled = false;
    }

    public void hostPressed()
    {
        manager.StartHost();
        hideCanvas();
    }

    public void clientPressed()
    {
        if (addressForClient != null && addressForClient != "")
        {
            manager.networkAddress = addressForClient;
        }
        else
        {
            manager.networkAddress = "localhost";
        }
        manager.StartClient();
        hideCanvas();
    }

    public void serverPressed()
    {
        manager.StartServer();
        hideCanvas();
    }

    private void hideCanvas()
    {
        camera_controller.enabled = true;
        gameObject.GetComponentInParent<Canvas>().gameObject.SetActive(false);
        if (Application.platform == RuntimePlatform.Android)
        {
            GameObject.Find("Main XR Rig").SetActive(false);
        }
    }
}
