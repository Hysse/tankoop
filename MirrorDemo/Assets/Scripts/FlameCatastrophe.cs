﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class FlameCatastrophe : NetworkBehaviour
{

    [Tooltip("The rate at which the Player's health will deplate while remainig inside the flames (Times per Second)")]
    [Range(1, 30)]
    public int tick_rate = 10;

    private int counter = 0;
    PlayerController playerController;

    // Start is called before the first frame update
    void Start()
    {
        playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.GetComponent<Renderer>().material.mainTextureOffset += new Vector2(0.05f * Time.deltaTime, 0.10f * Time.deltaTime);
    }

    public void OnTriggerStay(Collider other)
    {
        if(!isServer)
        {
            return;
        }
        counter++;
        if (other.CompareTag("Player") && (counter >= (60 / tick_rate)))
        {
            counter = 0;
            playerController.takeDamage(gameObject.GetComponent<Catastrophe>().getDamage());
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (!isServer)
        {
            return;
        }
        if (other.CompareTag("Player"))
        {
            counter = 0;
        }
    }
}
