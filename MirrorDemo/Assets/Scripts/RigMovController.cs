﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class RigMovController : NetworkBehaviour
{

    public GameObject vr_spawn;

    [Client]
    // Update is called once per frame
    void Update()
    {

        if(vr_spawn != null)
        {
            gameObject.transform.position = vr_spawn.transform.position;
        }
    }
}
