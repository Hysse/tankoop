﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class TanksNetworkManager : NetworkManager
{
    GameObject soldier;
    GameObject vr_rig;
    TankControl controlScript;
    TurretSync turretScript;
    RigMovController rigMovScript;

    public GameObject soldier_spawn;
    public GameObject PCGameOverMenu;
    public GameObject tank;
    public Camera MainCamera;

    public override void OnServerAddPlayer(NetworkConnection conn)
    {
        //ALLO STATO ATTUALE FUNZIONA SOLO SE IL PC PLAYER SI CONNETTE PRIMA DELL'OCULUS PLAYER (DA FIXARE)
        switch (numPlayers)
        {
            case 0:
                {
                    GameObject player = Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
                    controlScript = player.GetComponent<TankControl>();
                    NetworkServer.AddPlayerForConnection(conn, player);
                    tank = player;
                    soldier_spawn = GameObject.Find("Soldier Spawn");
                    TankCameraActivator cam_activator = tank.GetComponent<TankCameraActivator>();
                    cam_activator.my_cam = MainCamera;
                    if (MainCamera != null)
                    {
                        cam_activator.my_audio = MainCamera.GetComponent<AudioListener>();
                    }
                    GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
                    gameManager.PCGameOverMenu = PCGameOverMenu;
                    gameManager.deactivatePCGameOverMenu();
                    //GameObject catastropheSpawner = GameObject.Find("CatastropheSpawner");
                    //catastropheSpawner.GetComponent<CatastropheSpawner>().tank = tank; //PER TESTING, DA ELIMINARE IN SEGUITO!!!!
                    //catastropheSpawner.GetComponent<CatastropheSpawner>().startSpawning();
                    //GameObject pickupSpawner = GameObject.Find("PickupSpawner");
                    //pickupSpawner.GetComponent<PickupSpawner>().tank = tank;
                    //pickupSpawner.GetComponent<PickupSpawner>().startSpawning();
                    break;
                }
            case 1:
                {
                    if (tank != null)
                    {
                        soldier = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "Soldier"), soldier_spawn.transform.position, tank.transform.rotation);
                        NetworkServer.Spawn(soldier);
                        GameObject rig_spawn = GameObject.Find("VR_Spawn");
                        vr_rig = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "NetworkedRig"), rig_spawn.transform.position, tank.transform.rotation);
                        turretScript = vr_rig.GetComponent<TurretSync>();
                        turretScript.turret = GameObject.Find("Tower_Bone").gameObject;
                        turretScript.tank = tank;
                        rigMovScript = vr_rig.GetComponent<RigMovController>();
                        rigMovScript.vr_spawn = rig_spawn;
                        vr_rig.GetComponent<HandHeldController>().tank = tank;
                        GameObject catastropheSpawner = GameObject.Find("CatastropheSpawner");
                        catastropheSpawner.GetComponent<CatastropheSpawner>().tank = tank;
                        catastropheSpawner.GetComponent<CatastropheSpawner>().startSpawning();
                        GameObject pickupSpawner = GameObject.Find("PickupSpawner");
                        pickupSpawner.GetComponent<PickupSpawner>().tank = tank;
                        pickupSpawner.GetComponent<PickupSpawner>().startSpawning();
                    }
                    else
                    {
                        soldier = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "Soldier"), new Vector3(0, 1.4f, 0), Quaternion.identity);
                        NetworkServer.Spawn(soldier);
                        GameObject rig_spawn = GameObject.Find("VR_Spawn");
                        vr_rig = Instantiate(spawnPrefabs.Find(prefab => prefab.name == "NetworkedRig"), rig_spawn.transform.position, Quaternion.identity);
                    }
                    GameManager gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
                    gameManager.VRGameOverMenu = GameObject.FindWithTag("VRResults");
                    controlScript.soldier = soldier; //aggiunge il soldier al TankControl script per sincronizzare la position e la rotation dell'XRSoldier con i movimenti del Tank.
                    NetworkServer.AddPlayerForConnection(conn, vr_rig);
                    break;

                }
        }
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        SceneManager.LoadScene(0);
    }

    public override void OnStopHost()
    {
        base.OnStopHost();
        SceneManager.LoadScene(0);
    }

}
