﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class SeekerCatastrophe : NetworkBehaviour
{
    [Tooltip("The speed at which this Seeker will seek the player, if present")]
    [Range(1f, 30f)]
    public float seek_speed = 10f;

    [Tooltip("The explosion particle prefab that will activate when the Seeker reaches the player")]
    public GameObject explosion;

    public GameObject distortion;
    public GameObject seeker_block;

    CatastropheSpawner spawner;
    PlayerController playerController;

    GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        if (isServer)
        {
            spawner = GameObject.FindWithTag("CatSpawner").GetComponent<CatastropheSpawner>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(player != null)
        {
            float step = seek_speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            explosion.SetActive(true);
            seeker_block.GetComponent<Renderer>().enabled = false;
            distortion.SetActive(false);
            if (isServer)
            {
                StartCoroutine(TankoopUtils.destroyCoroutine(gameObject, 2, spawner));
                playerController.takeDamage(gameObject.GetComponent<Catastrophe>().getDamage());
            }
        }
    }
}
