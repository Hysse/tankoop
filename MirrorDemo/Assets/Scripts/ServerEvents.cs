﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ServerEvents : MonoBehaviour
{
    public PlayerController playerController;

    [Tooltip("The amount of time in seconds without being hit after which the health starts to regenerate")]
    [Range(1f, 20f)]
    public float regenDelay = 2f;

    [Tooltip("The amount of gained health per regen tick")]
    [Min(1f)]
    public float regenAmount = 5f;

    [Tooltip("The tick rate in seconds at which the regeneration happens")]
    [Min(10f)]
    public float regenTickRate = 20f;

    bool can_regen = false;
    Coroutine regenRoutine = null;
    Coroutine delayRegenRoutine = null;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if(can_regen && regenRoutine != null)
        {
            regenRoutine = StartCoroutine(regenCoroutine(regenTickRate));
        }
    }
    private IEnumerator regenCoroutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        if (playerController.getCurrentHealth() != playerController.startHealth)
        {
            playerController.heal(regenAmount);
        }
    }

    private IEnumerator regenDelayCoroutine(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        can_regen = true;
    }
}
