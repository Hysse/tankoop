﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxDebugController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject box;
    void Start()
    {
        if(Application.platform == RuntimePlatform.Android)
        {
            box.gameObject.SetActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
