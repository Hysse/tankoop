﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;
using System;
using UnityEngine.UI;

public class GameManager : NetworkBehaviour
{
    public Camera mainCamera;
    public GameObject VRGameOverMenu;
    public GameObject PCGameOverMenu;
    NetworkManager manager;

    private void Start()
    {
        manager = GameObject.Find("TanksNetworkManager").GetComponent<NetworkManager>();
        try
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                VRGameOverMenu = GameObject.FindWithTag("VRResults");
                deactivateVRGameOverMenu();
            }
        }
        catch (Exception e)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                Text logtxt = GameObject.FindWithTag("Log").GetComponent<Text>();
                if (logtxt != null)
                {
                    logtxt.text += e;
                }
            }
            else
            {
                Debug.Log(e.Message);
            }
        }
    }


    public void activatePCGameOverMenu(float finalScore)
    {
        try
        {
            PCGameOverMenu.SetActive(true);
            GameObject.Find("FinalScore").GetComponent<Text>().text = "Final score : " + finalScore;
        }
        catch (Exception e)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                Text logtxt = GameObject.FindWithTag("Log").GetComponent<Text>();
                if (logtxt != null)
                {
                    logtxt.text += e;
                }
            }
            else
            {
                Debug.Log(e.Message);
            }
        }
    }

    public void deactivatePCGameOverMenu()
    {
        try
        {
            PCGameOverMenu.SetActive(false);
        }
        catch (Exception e)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                Text logtxt = GameObject.FindWithTag("Log").GetComponent<Text>();
                if (logtxt != null)
                {
                    logtxt.text += e;
                }
            }
            else
            {
                Debug.Log(e.Message);
            }
        }
    }

    public void activateVRGameOverMenu(float finalScore)
    {
        try
        {
            VRGameOverMenu.SetActive(true);
            GameObject.Find("FinalScore").GetComponent<Text>().text = "Final score : " + finalScore;
        }
        catch (Exception e)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                Text logtxt = GameObject.FindWithTag("Log").GetComponent<Text>();
                if (logtxt != null)
                {
                    logtxt.text += e;
                }
            }
            else
            {
                Debug.Log(e.Message);
            }
        }
    }

    public void deactivateVRGameOverMenu()
    {
        try
        {
            VRGameOverMenu.SetActive(false);
        }
        catch (Exception e)
        {
            if (Application.platform == RuntimePlatform.Android)
            {
                Text logtxt = GameObject.FindWithTag("Log").GetComponent<Text>();
                if (logtxt != null)
                {
                    logtxt.text += e;
                }
            }
            else
            {
                Debug.Log(e.Message);
            }
        }
    }

    [ClientRpc(excludeOwner = false)]
    public void RpcStopGame()
    {
        // Destroys all the currently active catastrophes
        GameObject catSpawner = GameObject.Find("CatastropheSpawner");
        catSpawner.GetComponent<CatastropheSpawner>().stopSpawning();
        GameObject[] targets = GameObject.FindGameObjectsWithTag("Target");
        foreach (GameObject target in targets)
        {
            Destroy(target);
        }
        GameObject[] catastrophes = GameObject.FindGameObjectsWithTag("Catastrophe");
        foreach (GameObject catastrophe in catastrophes)
        {
            catSpawner.GetComponent<CatastropheSpawner>().deleteCatastrophe(catastrophe.GetComponent<Catastrophe>().getId());
        }

        // Destroys all the currently active pickups
        GameObject pickupSpawner = GameObject.Find("PickupSpawner");
        pickupSpawner.GetComponent<PickupSpawner>().stopSpawning();
        GameObject[] pickups = GameObject.FindGameObjectsWithTag("Pickup");
        foreach (GameObject pickup in pickups)
        {
            pickupSpawner.GetComponent<PickupSpawner>().deletePickup(pickup.GetComponent<Pickup>().getId());
        }
        GameObject.FindWithTag("Tank").GetComponent<TankControl>().enabled = false;
    }

    [Server]
    public void RestartGame()
    {
        NetworkServer.DisconnectAll();
        manager.StopHost();
    }

    [ClientRpc]
    public void RpcQuit()
    {
        Application.Quit();
    }
}
