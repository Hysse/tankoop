﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGUI : MonoBehaviour
{
    Transform initialPosition;

    // Start is called before the first frame update
    void Start()
    {
        initialPosition = gameObject.transform;
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = new Vector3(gameObject.transform.position.x, initialPosition.position.y, gameObject.transform.position.z);   
    }
}
