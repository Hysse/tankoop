﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Mirror;

public class CoinPickUp : NetworkBehaviour
{

    PlayerController playerController;
    PickupSpawner pickupSpawner;

    // Start is called before the first frame update
    void Start()
    {
        playerController = GameObject.Find("PlayerController").GetComponent<PlayerController>();
        if (isServer)
        {
            pickupSpawner = GameObject.Find("PickupSpawner").GetComponent<PickupSpawner>();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!isServer)
        {
            return;
        }
        if(other.CompareTag("Player"))
        {
            playerController.gainPoints(gameObject.GetComponent<Pickup>().getBenefitValue());
            pickupSpawner.deletePickup(gameObject.GetComponent<Pickup>().getId());
        }
    }
}
