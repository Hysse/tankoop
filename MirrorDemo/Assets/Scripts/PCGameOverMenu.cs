﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PCGameOverMenu : Singleton<PCGameOverMenu>
{

    public PCCameraController camera_controller;

    private void Start()
    {
        gameObject.GetComponent<Canvas>().worldCamera = GameObject.FindWithTag("MainCamera").GetComponent<Camera>();
        camera_controller.enabled = false;
    }

}
